package com.example.acer.resepku;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class register extends AppCompatActivity {
    Button punyaakun;
    private EditText txtEmailRegistration;
    private EditText txtPasswordRegistration;
    private FirebaseAuth mAuth;
    Button btnSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        punyaakun = (Button) findViewById(R.id.punyaakun);
        txtEmailRegistration = (EditText) findViewById(R.id.txtEmailRegistration);
        txtPasswordRegistration = (EditText) findViewById(R.id.txtPasswordRegistration);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        mAuth = FirebaseAuth.getInstance();

        Intent intent = getIntent();

        punyaakun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(register.this, login.class);
                register.this.startActivity(intent);
            }
        });



    }

    public void register(View view) {
        mAuth.createUserWithEmailAndPassword(txtEmailRegistration.getText().toString(), txtPasswordRegistration.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Adi", "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Intent intent = new Intent(register.this, login.class);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Adi", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(register.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}