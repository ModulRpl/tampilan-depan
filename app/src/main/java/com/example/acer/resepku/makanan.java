package com.example.acer.resepku;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class makanan extends AppCompatActivity {
    adapterMakanan adapter;
    RecyclerView data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makanan);
        adapter = new adapterMakanan(new adapterMakanan.itemClicked() {
            @Override
            public void onItemClicked(modelMakanan model) {
                Intent intent = new Intent(makanan.this, food.class);

                intent.putExtra("nama", model.nama);
                intent.putExtra("gambar", R.drawable.nasgor);
                intent.putExtra("ingredient", model.resep);

                makanan.this.startActivity(intent);
            }
        });
        data = findViewById(R.id.list_makanan);
        data.setLayoutManager(new LinearLayoutManager(this));
        data.addItemDecoration(new DividerItemDecoration(data.getContext(), DividerItemDecoration.VERTICAL));
        data.setHasFixedSize(true);
        data.setAdapter(adapter);

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                modelMakanan baru = dataSnapshot.getValue(modelMakanan.class);
                adapter.addData(baru);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}