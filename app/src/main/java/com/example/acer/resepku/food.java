package com.example.acer.resepku;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class food extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        getSupportActionBar().setTitle(getIntent().getStringExtra("nama"));
        ((ImageView)findViewById(R.id.gambar)).setImageResource(getIntent().getIntExtra("gambar", R.drawable.about));
        ((TextView)findViewById(R.id.articlefood)).setText(getIntent().getStringExtra("ingredient"));
    }
}
