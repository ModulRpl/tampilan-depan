package com.example.acer.resepku;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class homepage extends AppCompatActivity {
    ImageView makanann;
    ImageView minuman;
    ImageView about;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);

        Intent intent= getIntent();

        makanann = (ImageView)findViewById(R.id.makanann);
        minuman = (ImageView)findViewById(R.id.minuman);
        about = (ImageView)findViewById(R.id.about);
        makanann.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,makanan.class);
                homepage.this.startActivity(intent);
            }
        });
        minuman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,minuman2.class);
                homepage.this.startActivity(intent);
            }
        });


        ((ImageView)findViewById(R.id.resep)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homepage.this, TulisResep.class));
            }
        });
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(homepage.this,About.class);
                homepage.this.startActivity(intent);
            }
        });

    }
}